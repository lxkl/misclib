* [2023-10-15 Sun 21:08]

Now I realized that it is a nicer solution to put the key into
=.sgit/keys=, which I already did years before but forgot. But I am
keeping the changelog anyways.

* [2023-10-15 Sun 21:03]

Starting a changelog to sign with a new key.
