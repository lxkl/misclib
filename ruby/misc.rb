## user input

def gets_default(prompt, default)
  print "#{prompt} [#{default}]: "
  res = $stdin.gets.chomp
  if res == ""; default else res end
end

def gets_confirmation(prompt, default: false)
  x = gets_default(prompt, default ? "yes" : "no")
  x.downcase!
  ["yes", "ye", "y"].include?(x)
end
