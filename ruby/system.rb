require "open3"

def system_ok(*command, quiet: false, echo: false)
  puts command.join(" ") if echo
  if quiet
    system(*command, out: File::NULL, err: File::NULL)
  else
    system(*command)
  end
  unless $?.success?
    puts "fatal: unable to: #{command}"
    exit 111
  end
end

def system_try(*command, quiet: true, echo: false)
  puts command.join(" ") if echo
  if quiet
    system(*command, out: File::NULL, err: File::NULL)
  else
    system(*command)
  end
  $?.success?
end

def exec_ok(description=nil, ignore: [])
  res, stat = yield
  unless stat.success? || ignore.include?(stat.exitstatus)
    puts "fatal: exec_ok: unable to: #{description}"
    exit 111
  end
  res.chomp
end

def capture(*command, ignore: [])
  exec_ok(command.join(" "), ignore: ignore) { Open3.capture2e(*command) }
end

def pipe_into(content, *command)
  exec_ok(command.join(" ")) { Open3.capture2e(*command, stdin_data: content) }
end
