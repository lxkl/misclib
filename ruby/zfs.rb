def zfs_get(property, dataset)
  capture("zfs", "get", "-H", property, dataset).split(" ")[2]
end

def zfs_remount(dataset)
  system_try("zfs", "umount", dataset)
  system_ok("zfs", "mount", dataset)
end

def zfs_mounted_at?(dataset, mountpoint)
  zfs_get("mounted", dataset) == "yes" &&
    zfs_get("mountpoint", dataset) == mountpoint &&
    File.directory?("#{mountpoint}/.zfs/snapshot")
end

def zfs_datasets(dataset, type: "filesystem")
  capture("zfs", "list", "-H", "-t", type, "-r", dataset).split("\n").map { |l| l.split(" ")[0] }
end
