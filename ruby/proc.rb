def setuidgid(user, group: nil, chdir: false, skip_if_nonroot: false, &block)
  pw = (user.is_a? Integer) ? Etc.getpwuid(user) : Etc.getpwnam(user)
  if group.nil?
    gid = pw.gid
  elsif group.is_a? Integer
    gid = group
  else
    gid = Etc.getgrnam(group).gid
  end
  pid = Process.fork do
    Dir.chdir(pw.dir) if chdir
    unless skip_if_nonroot && Process.uid != 0
      Process.groups = []
      Process::GID.change_privilege(gid)
      Process::UID.change_privilege(pw.uid)
    end
    block.call
  end
  Process.wait(pid)
  unless $?.success?
    puts "fatal: no success running block as: #{user}"
    exit 111
  end
end
