class Timespec

  def initialize
    @spec = { "w" => { type: nil, value: nil, min: 0, max: nil },
              "d" => { type: nil, value: nil, min: 0, max: 6 },
              "h" => { type: nil, value: nil, min: 0, max: 23 },
              "m" => { type: nil, value: nil, min: 0, max: 59 },
              "s" => { type: nil, value: nil, min: 0, max: 59 },
            }
  end

  def parse(str)
    str.split(",").map do |x|
      unless x.match(/\A[wdhms][=\/][0-9]+\z/)
        puts "fatal: malformed timespec: #{str}"
        exit 100
      end
      s = @spec[x[0]]
      t = x[1] == "=" ? :eq : :div
      v = x[2..-1].to_i
      unless s[:min] <= v && (s[:max].nil? || v <= s[:max])
        puts "fatal: timespec range error: #{str}"
        exit 100
      end
      unless t != :div || v > 0
        puts "fatal: /0 not allowed in timespec: #{str}"
        exit 100
      end
      s[:type] = t
      s[:value] = v
    end
    self
  end

  def next_match(stamp)
    order = [ "w", "d", "h", "m", "s" ]
    res = { "w" => ((stamp.to_i + stamp.utc_offset + 4*24*60*60) / (7*24*60*60).to_f).floor,
            "d" => stamp.wday,
            "h" => stamp.hour,
            "m" => stamp.min,
            "s" => stamp.sec,
          }
    i = 0
    need_increase = false
    while i < order.length
      lower_reset = false
      u = order[i]
      case @spec[u][:type]
      when nil
        if need_increase
          if @spec[u][:max].nil? || res[u] < @spec[u][:max]
            res[u] += 1
            lower_reset = true
            need_increase = false
            i += 1
          else
            need_increase = true
            i -= 1
          end
        else
          i += 1
        end
      when :eq
        if res[u] < @spec[u][:value]
          res[u] = @spec[u][:value]
          lower_reset = true
          need_increase = false
          i += 1
        elsif res[u] >= @spec[u][:value] && need_increase
          if i == 0
            puts "fatal: there is no such time in the future"
            exit 100
          end
          i -= 1
        elsif res[u] > @spec[u][:value]
          need_increase = true
          i -= 1
        else
          i += 1
        end
      when :div
        x = res[u] % @spec[u][:value]
        if x > 0 || need_increase
          y = res[u] + (@spec[u][:value] - x)
          if ! @spec[u][:max].nil? && y > @spec[u][:max]
            need_increase = true
            i -= 1
          else
            res[u] = y
            lower_reset = true
            need_increase = false
            i += 1
          end
        else
          i += 1
        end
      end
      if lower_reset
        j = i
        while j < order.length
          u = order[j]
          res[u] = @spec[u][:min]
          j += 1
        end
      end
    end
    Time.at(res["w"] * 7*24*60*60 + res["d"] * 24*60*60 +
            res["h"] * 60*60 + res["m"] * 60 + res["s"] - stamp.utc_offset - 4*24*60*60)
  end

end
