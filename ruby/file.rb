require "fileutils"
require "securerandom"
require "tmpdir"

def via_tempfile(filename)
  if File.symlink?(filename)
    puts "fatal: via_tempfile does not work for symlinks"
    exit 111
  end
  tempfilename = filename + ".tmp." + SecureRandom.hex(32)
  FileUtils.rm_rf(tempfilename)
  FileUtils.touch(tempfilename)
  if File.exist?(filename)
    stat = File.stat(filename)
    FileUtils.chown(stat.uid, stat.gid, tempfilename)
    FileUtils.chmod(stat.mode, tempfilename)
  end
  File.open(tempfilename, "a") { |t| yield t }
  FileUtils.mv(tempfilename, filename)
end

def process_file(filename)
  content = File.read(filename)
  yield content
  via_tempfile(filename) { |t| t.write(content) }
end

def process_file_ln(filename)
  content = File.read(filename)
  via_tempfile(filename) do |t|
    content.each_line { |line| yield t, line.chomp }
  end
end
